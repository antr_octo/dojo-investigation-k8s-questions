from flask import Flask
import logging

app = Flask(__name__)

# Configure the logger to write to a file in the volume
logging.basicConfig(filename='/data/myapp.log', level=logging.INFO)

@app.route('/')
def hello_world():
    app.logger.info('Hello, world!')
    return 'Hello, world!'

if __name__ == '__main__':
    app.run(debug=True)