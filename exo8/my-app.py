from flask import Flask
import os

app = Flask(__name__)

@app.route('/')
def hello_world():
    return 'Hello world!'

@app.route('/healthCheck')
def health():
    return 'OK', 200

@app.route('/ready')
def ready():
    return 'I am ready'

if __name__ == '__main__':
    port = int(os.environ.get('PORT', 5000))
    app.run(debug=True, host='0.0.0.0', port=port)