from time import sleep
from flask import Flask
import logging

app = Flask(__name__)

@app.route('/')
def hello_world():
    sleep(180)
    app.logger.info('Hello, world!')
    return 'Hello, world!'

if __name__ == '__main__':
    app.run(debug=True)