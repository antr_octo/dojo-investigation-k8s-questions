from flask import Flask
import os

app = Flask(__name__)

# Récupère mes variables my_name & my_var depuis mon environnement
my_name = os.environ.get('my_name', 'default_value')
my_var = os.environ.get('my_var', 'NULL')

@app.route('/')
def hello_world():
    return 'Hello world!'

@app.route('/author')
def hello_people():
    return 'Hello world! I am ' + my_name

if __name__ == '__main__':
    port = int(os.environ.get('PORT', my_var))
    app.run(debug=True, host='0.0.0.0', port=port)